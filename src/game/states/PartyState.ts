import { IPartyModel } from "@/schemas/interface/IPartyModel";
import { ArraySchema, Schema, type } from "@colyseus/schema";
import { UserModel, UserState } from "./UserState";

export class PartyUserModel extends Schema {
  @type("string") team_name: string = "";
  @type("string") user_id: string = "";
  @type("string") username: string = "";
  @type("boolean") use_item_diamond = false;

  public EatDiamond() {
    this.use_item_diamond = true;
  }

  public IsPickTeam() {
    if (!this.team_name) {
      return false;
    }

    if (this.team_name.length < 1) {
      return false;
    }

    return true;
  }
}

export class PartyModel extends Schema {
  @type("string") party_id = "";
  @type("number") limit_size = 0;
  @type("string") name = "";
  @type("boolean") is_playing = false;
  @type("string") host_id = "";
  @type("number") current_size = 0;
  @type([PartyUserModel]) users_joined = new ArraySchema<PartyUserModel>();

  public Play() {
    if (!this.is_playing) {
      this.is_playing = true;
    }
  }

  public ChangeHost(user_id: string) {
    let joined = false;

    for (const userJoined of this.users_joined) {
      if (userJoined.user_id === user_id) {
        joined = true;
        break;
      }
    }

    if (!joined) {
      throw new Error("User not in party");
    }

    this.host_id = user_id;
  }

  public Exit(userID: string) {
    const userIndex = this.users_joined.findIndex((v) => v.user_id === userID);

    if (userIndex < 0) {
      throw new Error("User not in party");
    }

    this.users_joined.splice(userIndex, 1);
    this.current_size = this.users_joined.length;
    console.log(`User ${userID} exited: ${this.current_size}`);
    return true;
  }

  public Join(user: UserModel) {
    if (this.is_playing) {
      throw new Error("Party is playing");
    }

    if (this.current_size >= this.limit_size) {
      throw new Error("Party is full");
    }

    if (this.IsUserJoined(user.user_id)) {
      throw new Error("User joined in party");
    }

    const partyUser = new PartyUserModel();
    partyUser.user_id = user.user_id;
    partyUser.username = user.username;

    this.users_joined.push(partyUser);
    this.current_size = this.users_joined.length;
    console.log(`New user ${user.user_id} joined party: ${this.current_size}`);
    return true;
  }

  public ChooseTeam(userID: string, teamName: string) {
    if (this.is_playing) {
      throw new Error("Party is playing");
    }

    if (!this.IsUserJoined(userID)) {
      throw new Error("User joined in party");
    }

    const partyUserIndex = this.users_joined.findIndex(
      (v) => v.user_id === userID
    );

    const partyUser = this.users_joined[partyUserIndex];
    partyUser.team_name = teamName;
    this.users_joined[partyUserIndex] = partyUser;

    console.log(`User ${userID} pick team ${teamName}`);
    return true;
  }

  private IsUserJoined(userID: string) {
    for (const userJoined of this.users_joined) {
      if (userJoined.user_id === userID) {
        return true;
      }
    }

    return false;
  }
}

export class PartyState extends Schema {
  @type([PartyModel]) all_party = new ArraySchema<PartyModel>();

  public Create(model: IPartyModel, user: UserModel): PartyModel {
    if (!user) {
      throw new Error("User dont exist");
    }

    const party = new PartyModel();
    party.party_id = model._id?.toString();
    party.limit_size = model.LimitSize;
    party.name = model.Name;
    party.host_id = user.user_id;
    party.is_playing = false;

    this.all_party.push(party);
    console.log(`New party ${party.party_id} created`);
    return party;
  }

  public Delete(party: PartyModel) {
    const index = this.all_party.indexOf(party);

    if (index >= 0) {
      this.all_party.splice(index, 1);
      console.log(`Party ${party.party_id} deleted`);
    } else {
      console.log(`Party ${party.party_id} not found`);
    }
  }
}
