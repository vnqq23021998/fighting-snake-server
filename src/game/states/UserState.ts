import { EItemType, EUserState } from "@/common/_Enums";
import { UserUpdatePosRequest } from "@/models/request/UserRequest";
import { IUserModel } from "@/schemas/interface/IUserModel";
import { ArraySchema, Schema, type } from "@colyseus/schema";

export class Vec3 extends Schema {
  @type("number") x = 0;
  @type("number") y = 0;

  constructor(x: number, y: number) {
    super();
    this.x = x;
    this.y = y;
  }

  public Add(ratio: number) {
    this.x += ratio;
    this.y += ratio;
  }

  public Multiply(ratio: number) {
    this.x *= ratio;
    this.y *= ratio;
  }

  public BetweenDistance(endPos: Vec3) {
    const dx = endPos.x - this.x;
    const dy = endPos.y - this.y;
    return Math.sqrt(dx * dx + dy * dy);
  }
}

export class UserModel extends Schema {
  @type("string") user_id = "";
  @type("string") party_id = "";
  @type("string") session_id = "";
  @type("string") username = "";
  @type("number") balance = 0;
  @type("number") move_speed = 10;
  @type("number") origin_move_speed = 10;
  @type("number") item_stealth = 0;
  @type("number") item_acceleration = 0;
  @type("boolean") use_item_diamond = false;
  @type("boolean") is_death = false;
  @type("string") rewarded_time = "";
  @type("number") state: EUserState = EUserState.Idle;
  @type(Vec3) current_pos = new Vec3(0, 0);

  public UseStealth() {
    if (this.item_stealth - 1 >= 0) {
      this.item_stealth -= 1;
      return true;
    }

    return false;
  }

  public UseAcceleration() {
    if (this.item_acceleration - 1 >= 0) {
      this.item_acceleration -= 1;
      this.origin_move_speed = this.move_speed;
      this.move_speed = (this.move_speed * 50) / 100;
      return true;
    }

    return false;
  }

  public GetRewardDay() {
    this.item_stealth += 3;
    this.item_acceleration += 3;
    this.rewarded_time = new Date().toLocaleDateString();
  }

  public PartyPlay(partyID: string, initPos: Vec3) {
    this.party_id = partyID;
    this.current_pos = new Vec3(initPos.x, initPos.y);
    this.state = EUserState.Idle;
  }

  public Eat(item: EItemType) {
    switch (item) {
      case EItemType.Diamond:
        this.balance += 100;
        break;

      case EItemType.Acceleration:
        this.item_acceleration += 1;
        break;

      case EItemType.Stealth:
        this.item_stealth += 1;
        break;
    }
  }

  public Attack(target: UserModel) {
    if (!this.use_item_diamond) {
      return false;
    }

    target.Death();
    return true;
  }

  public Death() {
    this.is_death = true;
  }

  public UpdatePos(request: UserUpdatePosRequest) {
    this.state = request.State;
    this.current_pos = new Vec3(request.EndPosX, request.EndPosY);
    // const endPos = new Vector3(request.EndPosX, request.EndPosY);
    // const newPos = new Vector3(this.current_pos.x, this.current_pos.y);

    // newPos.Add(this.move_speed);
    // const distance = newPos.BetweenDistance(endPos);

    // console.log(
    //   `Distance between ${JSON.stringify(this.current_pos)} - ${JSON.stringify(
    //     endPos
    //   )}: ${distance}`
    // );

    // if (distance < 1) {
    //   this.current_pos = new Vector3(endPos.x, endPos.y);
    // } else {
    //   // detect có thể bị hack speed
    //   this.current_pos = new Vector3(newPos.x, newPos.y);
    // }
  }
}

export class UserState extends Schema {
  @type([UserModel]) all_users = new ArraySchema<UserModel>();
  @type("number") current_reconnection_round = 0;

  public Create(model: IUserModel): UserModel {
    const user = new UserModel();
    user.user_id = model._id?.toString();
    user.username = model.Username;
    user.balance = model.Balance;
    user.move_speed = model.MoveSpeed;
    user.state = EUserState.Idle;

    this.all_users.push(user);
    console.log(`New user ${user.user_id} created`);
    return user;
  }
}
