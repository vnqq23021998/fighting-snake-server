import { JWT_SECRET_KEY } from "@/config/EnvConfig";
import { ITokenData } from "@/models/TokenData";
import { Client, Room, Server, ServerError } from "colyseus";
import jwt from "jsonwebtoken";
import { UserModel, UserState } from "../states/UserState";
import { UserService } from "@/services/UserService";
import { Constant } from "@/common/_Constant";
import { Logger } from "@/common/Logger";
import { ConvertAndValidation, ConvertToClass } from "@/common/Validation";
import {
  UserAttackRequest,
  UserEatRequest,
  UserUpdatePosRequest,
} from "@/models/request/UserRequest";
import { PartyModel } from "../states/PartyState";
import * as cron from "node-cron";
import { EItemType } from "@/common/_Enums";

export class UserRoom extends Room<UserState> {
  private _userService: UserService = new UserService();

  onAuth(client: Client<ITokenData>, options: { token: string }) {
    if (options && options.token) {
      try {
        const data = jwt.verify(options.token, JWT_SECRET_KEY) as ITokenData;

        if (data) {
          client.userData = { ID: data.ID, Username: data.Username };
          console.log(`New user ${client.userData?.ID} connected to UserRoom`);
          return data;
        }

        throw new ServerError(400, "Access Token not valid");
      } catch (err) {
        Logger.error(err);
        throw new ServerError(400, "Bad access token");
      }
    } else {
      throw new ServerError(400, "Token not provided");
    }
  }

  onCreate() {
    this.setState(new UserState());

    cron.schedule("0 0 * * *", () => {
      for (const client of this.clients) {
        const userID = client.userData?.ID;
        this.SendRewardDay(client, userID);
      }
    });

    this.onMessage(
      "USER::UPDATE_POS",
      async (client: Client<ITokenData>, message: string) => {
        try {
          console.log("Data received:", message);
          const request = ConvertAndValidation(message, UserUpdatePosRequest);
          const userID = client.userData?.ID;

          const userValue = await this.presence.hget(
            Constant.REDIS_ALL_USERS,
            userID
          );

          const user = ConvertToClass(userValue, UserModel);

          if (!user) {
            throw new Error("User not found");
          }

          const partyValue = await this.presence.hget(
            Constant.REDIS_ALL_PARTIES,
            user.party_id
          );

          const party = ConvertToClass(partyValue, PartyModel);

          if (!party) {
            throw new Error("User dont join any party");
          }

          if (!party.is_playing) {
            throw new Error("Party is not playing");
          }

          user.UpdatePos(request);

          const json = JSON.stringify({
            user_id: userID,
            current_pos: user.current_pos,
            current_state: user.state,
          });

          for (const userJoined of party.users_joined) {
            const client = this.clients.find((v) => {
              return v.userData?.ID === userJoined.user_id;
            });

            if (client) {
              client.send("USER::UPDATE_POS", json);
            } else {
              console.log("Not found user", userJoined.user_id);
            }
          }
        } catch (err) {
          Logger.error("USER::UPDATE_POS", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "USER::ATTACK",
      async (client: Client<ITokenData>, message: string) => {
        try {
          console.log("Data received:", message);
          const request = ConvertAndValidation(message, UserAttackRequest);
          const userID = client.userData?.ID;

          const userValue = await this.presence.hget(
            Constant.REDIS_ALL_USERS,
            userID
          );

          const userTargetValue = await this.presence.hget(
            Constant.REDIS_ALL_USERS,
            request.TargetUserID
          );

          const user = ConvertToClass(userValue, UserModel);
          const targetUser = ConvertToClass(userTargetValue, UserModel);

          if (!user) {
            throw new Error("User not found");
          }

          if (!targetUser) {
            throw new Error("Target user not found");
          }

          const partyValue = await this.presence.hget(
            Constant.REDIS_ALL_PARTIES,
            user.party_id
          );

          const partyTargetValue = await this.presence.hget(
            Constant.REDIS_ALL_PARTIES,
            targetUser.party_id
          );

          const party = ConvertToClass(partyValue, PartyModel);
          const partyTarget = ConvertToClass(partyTargetValue, PartyModel);

          if (!party) {
            throw new Error("User dont join any party");
          }

          if (!partyTarget) {
            throw new Error("Target user dont join any party");
          }

          if (!party.is_playing) {
            throw new Error("Party is not playing");
          }

          if (party.party_id !== partyTarget.party_id) {
            throw new Error("Can't attack user in other party");
          }

          const isAttacked = user.Attack(targetUser);

          if (isAttacked) {
            const json = JSON.stringify({ user_id: targetUser.user_id });

            for (const userJoined of party.users_joined) {
              const client = this.clients.find((v) => {
                return v.userData?.ID === userJoined.user_id;
              });

              if (client) {
                client.send("USER::PARTY_BEING_ATTACK", json);
              } else {
                console.log("Not found user", userJoined.user_id);
              }
            }
          } else {
            console.log(
              `User: ${userID} attack user: ${targetUser.user_id} failed`
            );
          }
        } catch (err) {
          Logger.error("USER::ATTACK", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "USER::REWARD_DAY",
      async (client: Client<ITokenData>, message: string) => {
        try {
          console.log("Data received:", message);
          this.SendRewardDay(client, client.userData?.ID);
        } catch (err) {
          Logger.error("USER::REWARD_DAY", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "USER::EAT",
      async (client: Client<ITokenData>, message: string) => {
        try {
          console.log("Data received:", message);
          const request = ConvertAndValidation(message, UserEatRequest);
          const userID = client.userData?.ID;

          const userValue = await this.presence.hget(
            Constant.REDIS_ALL_USERS,
            userID
          );

          const user = ConvertToClass(userValue, UserModel);

          if (!user) {
            throw new Error("User not found");
          }

          const partyValue = await this.presence.hget(
            Constant.REDIS_ALL_PARTIES,
            user.party_id
          );

          const party = ConvertToClass(partyValue, PartyModel);

          if (!party) {
            throw new Error("User dont join any party");
          }

          if (!party.is_playing) {
            throw new Error("Party is not playing");
          }

          user.Eat(request.Type);

          if (request.Type === EItemType.Diamond) {
            let teamName = "";

            for (const u of party.users_joined) {
              if (u.user_id === userID) {
                teamName = u.team_name;
                break;
              }
            }

            if (teamName.length > 0) {
              const json = JSON.stringify({ evolution: true });

              for (const u of party.users_joined) {
                if (u.team_name === teamName) {
                  const client = this.clients.find((v) => {
                    return v.userData?.ID === u.user_id;
                  });

                  if (client) {
                    client.send("USER::EVOLUTION", json);
                  }

                  u.EatDiamond();
                }
              }
            }
          } else {
            client.send("USER::EAT", JSON.stringify({ eat: true }));
          }
        } catch (err) {
          Logger.error("USER::EAT", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );
  }

  async onJoin(client: Client<ITokenData>): Promise<any> {
    const userID = client.userData?.ID;
    const user = await this._userService.GetUser(userID);

    if (!user) {
      throw new ServerError(400, "Not found user");
    }

    const newUser = this.state.Create(user);

    await this.presence.hset(
      Constant.REDIS_ALL_USERS,
      userID,
      JSON.stringify(newUser)
    );

    console.log(client.sessionId, "Joined!");
  }

  async onLeave(client: Client<ITokenData>, consented: boolean) {
    // flag client as inactive for other users
    // this.state.all_user.get(client.sessionId).connected = false;

    try {
      if (consented) {
        throw new Error("consented leave");
      }

      //
      // Get reconnection token
      // NOTE: do not use `await` here yet
      //
      const reconnection = this.allowReconnection(client, "manual");

      //
      // here is the custom logic for rejecting the reconnection.
      // for demonstration purposes of the API, an interval is created
      // rejecting the reconnection if the player has missed 2 rounds,
      // (assuming he's playing a turn-based game)
      //
      // in a real scenario, you would store the `reconnection` in
      // your Player instance, for example, and perform this check during your
      // game loop logic
      //
      const currentRound = this.state.current_reconnection_round;

      const interval = setInterval(() => {
        if (this.state.current_reconnection_round - currentRound > 2) {
          // manually reject the client reconnection
          reconnection.reject();
          clearInterval(interval);
        }
      }, 1000);

      // now it's time to `await` for the reconnection
      await reconnection;

      // client returned! let's re-activate it.
    } catch (err) {
      // reconnection has been rejected. let's remove the client.
      Logger.error(err);
    }
  }

  onDispose() {
    console.log("USER ROOM", this.roomId, "Disposing...");
  }

  private async SendRewardDay(client: Client<ITokenData>, userID: string) {
    const userValue = await this.presence.hget(
      Constant.REDIS_ALL_USERS,
      userID
    );

    const user = ConvertToClass(userValue, UserModel);

    if (user) {
      console.log("Sent reward day for user " + userID);
      user.GetRewardDay();
      const json = JSON.stringify(user);
      client.send("USER::REWARD_DAY", json);
      await this.presence.hset(Constant.REDIS_ALL_USERS, userID, json);
    } else {
      console.log("Send reward day failed to user " + userID);
    }
  }
}
