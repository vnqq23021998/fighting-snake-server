import { JWT_SECRET_KEY } from "@/config/EnvConfig";
import { ITokenData } from "@/models/TokenData";
import { Client, Room, ServerError } from "colyseus";
import jwt from "jsonwebtoken";
import { PartyState, PartyUserModel } from "../states/PartyState";
import { Logger } from "@/common/Logger";
import { ConvertAndValidation, ConvertToClass } from "@/common/Validation";
import {
  ChooseTeamPartyRequest,
  CreatePartyRequest,
  PartyRequest,
} from "@/models/request/PartyRequest";
import { PartyService } from "@/services/PartyService";
import { Constant, RandomNumber } from "@/common/_Constant";
import { UserModel, Vec3 } from "../states/UserState";

export class PartyRoom extends Room<PartyState> {
  private _partyService: PartyService = new PartyService();

  onAuth(client: Client<ITokenData>, options: { token: string }) {
    if (options && options.token) {
      try {
        const data = jwt.verify(options.token, JWT_SECRET_KEY) as ITokenData;

        if (data) {
          client.userData = { ID: data.ID, Username: data.Username };
          console.log(`New user ${client.userData?.ID} connected to PartyRoom`);

          for (const c of this.clients) {
            console.log("Client:", c.userData?.ID, c.userData?.Username);
          }

          return data;
        }

        throw new ServerError(400, "Access Token not valid");
      } catch (err) {
        Logger.error(err);
        throw new ServerError(400, "Bad access token");
      }
    } else {
      throw new ServerError(400, "Token not provided");
    }
  }

  onCreate() {
    this.setState(new PartyState());

    this.onMessage(
      "PARTY::CREATE",
      async (client: Client<ITokenData>, message: string) => {
        try {
          const request = ConvertAndValidation(message, CreatePartyRequest);
          const result = await this._partyService.Create(request);

          if (result) {
            const userID = client.userData?.ID;

            const userValue = await this.presence.hget(
              Constant.REDIS_ALL_USERS,
              userID
            );

            const user = ConvertToClass(userValue, UserModel);

            if (!user) {
              throw new Error("User not found");
            }

            const party = this.state.Create(result, user);
            party.Join(user);
            client.send("PARTY::CREATE", JSON.stringify(party));
          } else {
            throw new Error("Tạo party thất bại");
          }
        } catch (err) {
          Logger.error("PARTY::CREATE", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "PARTY::PLAY",
      async (client: Client<ITokenData>, message: string) => {
        try {
          const request = ConvertAndValidation(message, PartyRequest);
          const userID = client.userData?.ID;

          const party = this.state.all_party.find(
            (v) => v.party_id === request.PartyID
          );

          if (!party) {
            throw new Error("Party not found");
          }

          if (party.host_id !== userID) {
            throw new Error("You are not host of this party");
          }

          for (const user of party.users_joined) {
            if (!user.IsPickTeam()) {
              throw new Error(
                "Không thể bắt đầu chơi mà tất cả người chơi chưa chọn team"
              );
            }
          }

          party.Play();

          if (party.is_playing) {
            const usersInParty = [];
            console.log(`Party ${party.party_id} is playing`);

            this.presence.hset(
              Constant.REDIS_ALL_PARTIES,
              party.party_id,
              JSON.stringify(party)
            );

            for (const userJoined of party.users_joined) {
              const userValue = await this.presence.hget(
                Constant.REDIS_ALL_USERS,
                userJoined.user_id
              );

              const user = ConvertToClass(userValue, UserModel);

              if (user) {
                const initPos = new Vec3(
                  RandomNumber(-5, 5),
                  RandomNumber(-3, 3)
                );

                user.PartyPlay(party.party_id, initPos);
                usersInParty.push(user);

                this.presence.hset(
                  Constant.REDIS_ALL_USERS,
                  user.user_id,
                  JSON.stringify(user)
                );
              }
            }

            if (usersInParty.length < 1) {
              throw new Error("Không thể khởi tạo game");
            }

            const json = JSON.stringify(usersInParty);

            for (const userJoined of party.users_joined) {
              const client = this.clients.find(
                (v) =>
                  v.userData?.ID === userJoined.user_id &&
                  userJoined.user_id !== userID
              );

              if (client) {
                client.send("PARTY::PLAY", json);
              }
            }

            client.send("PARTY::PLAY", json);
          } else {
            throw new Error("Party play error");
          }
        } catch (err) {
          Logger.error("PARTY::PLAY", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "PARTY::JOIN",
      async (client: Client<ITokenData>, message: string) => {
        try {
          const request = ConvertAndValidation(message, PartyRequest);

          const party = this.state.all_party.find(
            (v) => v.party_id === request.PartyID
          );

          if (party) {
            const userID = client.userData?.ID;

            const userValue = await this.presence.hget(
              Constant.REDIS_ALL_USERS,
              userID
            );

            const user = ConvertToClass(userValue, UserModel);

            if (!user) {
              throw new Error("User not found");
            }

            party.Join(user);
            const json = JSON.stringify(party);

            for (const userJoined of party.users_joined) {
              const client = this.clients.find(
                (v: Client<ITokenData>) =>
                  v.userData?.ID === userJoined.user_id &&
                  userJoined.user_id !== userID
              );

              if (client) {
                client.send("PARTY::JOIN", json);
              }
            }

            client.send("PARTY::JOIN", json);
          } else {
            throw new Error("Tham gia party thất bại");
          }
        } catch (err) {
          Logger.error("PARTY::JOIN", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "PARTY::CHOOSE_TEAM",
      async (client: Client<ITokenData>, message: string) => {
        try {
          const request = ConvertAndValidation(message, ChooseTeamPartyRequest);

          const party = this.state.all_party.find(
            (v) => v.party_id === request.PartyID
          );

          if (party) {
            const userID = client.userData?.ID;
            party.ChooseTeam(userID, request.TeamName);

            const userValue = await this.presence.hget(
              Constant.REDIS_ALL_USERS,
              userID
            );

            const user = ConvertToClass(userValue, UserModel);

            if (!user) {
              throw new Error("User not found");
            }

            const partyUser = new PartyUserModel();
            partyUser.user_id = user.user_id;
            partyUser.username = user.username;
            partyUser.team_name = request.TeamName;
            let countTeamA = 0;
            let countTeamB = 0;

            for (const userJoined of party.users_joined) {
              switch (userJoined.team_name?.toLowerCase()) {
                case "a":
                  countTeamA++;
                  break;

                case "b":
                  countTeamB++;
                  break;
              }

              if (countTeamA > 4) {
                throw new Error("Bạn không thể chọn team A vì đã đủ 4 người");
              }

              if (countTeamB > 4) {
                throw new Error("Bạn không thể chọn team B vì đã đủ 4 người");
              }
            }

            const json = JSON.stringify(partyUser);

            for (const userJoined of party.users_joined) {
              const client = this.clients.find(
                (v) =>
                  v.userData?.ID === userJoined.user_id &&
                  userJoined.user_id !== userID
              );

              if (client) {
                client.send("PARTY::CHOOSE_TEAM", json);
              }
            }

            client.send("PARTY::CHOOSE_TEAM", json);
          } else {
            throw new Error("Chọn team thất bại");
          }
        } catch (err) {
          Logger.error("PARTY::CHOOSE_TEAM", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage(
      "PARTY::EXIT",
      async (client: Client<ITokenData>, message: string) => {
        try {
          const request = ConvertAndValidation(message, PartyRequest);

          const party = this.state.all_party.find(
            (v) => v.party_id === request.PartyID
          );

          if (party) {
            const userID = client.userData?.ID;
            party.Exit(userID);

            if (party.current_size > 0) {
              if (party.host_id === userID) {
                party.ChangeHost(party.users_joined[0].user_id);
              }
            }

            if (party.users_joined.length > 0) {
              for (const userJoined of party.users_joined) {
                const client = this.clients.find(
                  (v) =>
                    v.userData?.ID === userJoined.user_id &&
                    userJoined.user_id !== userID
                );

                if (client) {
                  client.send("PARTY::EXIT", userID);
                }
              }
            } else {
              this.state.Delete(party);
            }

            client.send("PARTY::EXIT", userID);
          } else {
            throw new Error("Thoát party thất bại");
          }
        } catch (err) {
          Logger.error("PARTY::EXIT", err);
          client.send("ERROR", `Lỗi: ${err?.message}`);
        }
      }
    );

    this.onMessage("PARTY::ALL", async (client: Client<ITokenData>) => {
      try {
        client.send(
          "PARTY::ALL",
          JSON.stringify(this.state.all_party.filter((v) => !v.is_playing))
        );
      } catch (err) {
        Logger.error("PARTY::ALL", err);
        client.send("ERROR", `Lỗi: ${err?.message}`);
      }
    });
  }

  async onJoin(client: Client<ITokenData>): Promise<any> {
    console.log(client.sessionId, "Joined!");
  }

  onLeave(client: Client, consented: boolean) {
    console.log(client.sessionId, "Left!");
  }

  onDispose() {
    console.log("PARTY ROOM", this.roomId, "Disposing...");
  }
}
