import { App } from "./App";

process.on("uncaughtException", console.error);
process.on("unhandledRejection", console.error);

const app = new App();
app.Listen();
