import mongoose from "mongoose";
import { IUserModel, IUserModelMethod } from "./interface/IUserModel";
import { EUserState } from "@/common/_Enums";

const UserSchema = new mongoose.Schema<
  IUserModel,
  IUserModelMethod,
  IUserModelMethod
>({
  Username: { type: String, default: "" },
  Password: { type: String, default: "" },
  Balance: { type: Number, default: 0 },
  State: { type: Number, default: EUserState.Idle },
  MoveSpeed: { type: Number, default: 10 },
});

UserSchema.methods.GetUserFromID = async (userID: string) => {
  return await UserModel.findOne({ _id: userID });
};

UserSchema.methods.GetUser = async (username: string, password: string) => {
  return await UserModel.findOne({ Username: username, Password: password });
};

export const UserModel = mongoose.model<IUserModel>("tb_users", UserSchema);
