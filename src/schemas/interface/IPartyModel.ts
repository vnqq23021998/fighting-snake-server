import { Document, Model } from "mongoose";

export interface IPartyModelMethod extends Model<IPartyModel> {
  GetRoom(_id: string): Promise<IPartyModel | null>;
}

export interface IPartyModel extends Document {
  LimitSize: number;
  Name: string;
}
