import { EUserState } from "@/common/_Enums";
import { Document, Model } from "mongoose";

export interface IUserModelMethod extends Model<IUserModel> {
  GetUser(username: string, password: string): Promise<IUserModel | null>;
  GetUserFromID(userID: string): Promise<IUserModel | null>;
}

export interface IUserModel extends Document {
  Username: string;
  Password: string;
  Balance: number;
  State: EUserState;
  MoveSpeed: number;
}
