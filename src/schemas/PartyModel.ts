import mongoose from "mongoose";
import { IPartyModel, IPartyModelMethod } from "./interface/IPartyModel";
import { PARTY_MAX_SIZE } from "@/config/EnvConfig";

const Schema = new mongoose.Schema<
  IPartyModel,
  IPartyModelMethod,
  IPartyModelMethod
>({
  LimitSize: { type: Number, default: Number(PARTY_MAX_SIZE) },
  Name: { type: String, default: "" },
});

Schema.methods.GetRoom = async (_id: string) => {
  return await PartyModel.findOne({ _id: _id });
};

export const PartyModel = mongoose.model<IPartyModel>("tb_parties", Schema);
