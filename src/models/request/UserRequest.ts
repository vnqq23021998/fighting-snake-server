import { EItemType, EUserState } from "@/common/_Enums";
import { IsDefined, IsEnum, IsNumber, IsString } from "class-validator";

export class UserUpdatePosRequest {
  @IsDefined()
  @IsEnum(EUserState)
  public State: EUserState;

  @IsDefined()
  @IsNumber()
  public EndPosX: number;

  @IsDefined()
  @IsNumber()
  public EndPosY: number;
}

export class UserAttackRequest {
  @IsDefined()
  @IsString()
  public TargetUserID: string;
}

export class UserEatRequest {
  @IsDefined()
  @IsEnum(EItemType)
  public Type: EItemType;
}
