import { IsDefined, IsNotEmpty, IsString } from "class-validator";

export class PartyRequest {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  public PartyID: string;
}

export class CreatePartyRequest {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  public Name: string;
}

export class ChooseTeamPartyRequest extends PartyRequest {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  public TeamName: string;
}
