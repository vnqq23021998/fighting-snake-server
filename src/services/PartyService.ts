import { PartyModel } from "@/schemas/PartyModel";
import { IPartyService } from "./interfaces/IPartyService";
import { CreatePartyRequest } from "@/models/request/PartyRequest";
import { IPartyModel } from "@/schemas/interface/IPartyModel";

export class PartyService implements IPartyService {
  async Create(request: CreatePartyRequest): Promise<IPartyModel> {
    let currentParty = await PartyModel.findOne({ Name: request.Name });

    if (currentParty) {
      return undefined;
    }

    let result = await PartyModel.create({ Name: request.Name });
    return result;
  }

  
}
