import { IUserModel } from "@/schemas/interface/IUserModel";

export interface IUserService {
  GetUser(userID: string): Promise<IUserModel>;
}
