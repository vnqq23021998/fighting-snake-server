import { CreatePartyRequest } from "@/models/request/PartyRequest";
import { IPartyModel } from "@/schemas/interface/IPartyModel";

export interface IPartyService {
  Create(request: CreatePartyRequest): Promise<IPartyModel>;
}
