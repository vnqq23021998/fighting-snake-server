import { IUserModel } from "@/schemas/interface/IUserModel";
import { IUserService } from "./interfaces/IUserService";
import { UserModel } from "@/schemas/UserModel";

export class UserService implements IUserService {
  async GetUser(userID: string): Promise<IUserModel> {
    return await UserModel.findOne({ _id: userID });
  }
}
