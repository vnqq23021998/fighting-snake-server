import { Router } from "express";
import { Routes } from "../Routes";
import { UserController } from "@/controllers/users/UserController";

export class UserRoute implements Routes {
  public path = "users";
  public router = Router();
  public userController = new UserController();

  constructor() {
    // POST
    this.router.post("/login", this.userController.Login);
    this.router.post("/register", this.userController.Register);
  }
}
