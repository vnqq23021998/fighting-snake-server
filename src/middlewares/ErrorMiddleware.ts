import { Logger } from "@/common/Logger";
import { BadRequest } from "@/common/Response";
import { NextFunction, Request, Response } from "express";

export const ErrorMiddleware = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  Logger.error(err);
  return BadRequest(res, err.message);
};
