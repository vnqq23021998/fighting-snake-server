import { Create } from "@/common/Response";
import { HttpStatusCode } from "axios";
import { Request, Response } from "express";

export const NotFoundMiddleware = (req: Request, res: Response) => {
  return Create(
    res,
    HttpStatusCode.NotFound,
    `API not found by URL ${req.url}`,
    null
  );
};
