import { Json, Success, BadRequest } from "@/common/Response";
import { JWT_SECRET_KEY } from "@/config/EnvConfig";
import { ITokenData } from "@/models/TokenData";
import { UserModel } from "@/schemas/UserModel";
import { IUserModel } from "@/schemas/interface/IUserModel";
import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

export class UserController {
  public async Login(
    req: Request<{}, {}, IUserModel>,
    res: Response<Json<string>>,
    next: NextFunction
  ) {
    try {
      if (!req.body.Username || !req.body.Password) {
        return BadRequest(res, "Dữ liệu truyền lên thiếu");
      }

      const user = await UserModel.findOne({
        Username: req.body.Username,
        Password: req.body.Password,
      });

      if (!user) {
        return BadRequest(res, "Đăng nhập thất bại");
      }

      const data: ITokenData = {
        Username: user.Username,
        ID: user._id?.toString(),
      };

      const token = jwt.sign(data, JWT_SECRET_KEY);
      res.setHeader("Authorization", token);
      return Success(res, data.ID);
    } catch (err) {
      next(err);
    }
  }

  public async Register(
    req: Request<{}, {}, IUserModel>,
    res: Response<Json<string>>,
    next: NextFunction
  ) {
    try {
      if (!req.body.Username || !req.body.Password) {
        return BadRequest(res, "Dữ liệu gửi lên bị thiếu");
      }

      const user = await UserModel.findOne({
        Username: req.body.Username,
        Password: req.body.Password,
      });

      if (user) {
        return BadRequest(res, "Tên tài khoản đã tồn tại");
      }

      const result = await UserModel.create({
        Username: req.body.Username,
        Password: req.body.Password,
      });

      if (!result) {
        return BadRequest(res, "Đăng ký lỗi");
      }

      return Success(res, "Đăng ký thành công");
    } catch (err) {
      next(err);
    }
  }
}
