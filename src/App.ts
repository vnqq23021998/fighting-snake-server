import { RedisPresence, Server } from "colyseus";
import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import hpp from "hpp";
import {
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_PORT,
  DB_USER,
} from "./config/EnvConfig";
import { createServer } from "http";
import { monitor } from "@colyseus/monitor";
import cookieParser from "cookie-parser";
import compression from "compression";
import helmet from "helmet";
import { Routes } from "./routes/Routes";
import { UserRoute } from "./routes/users/UserRoute";
import { ErrorMiddleware } from "./middlewares/ErrorMiddleware";
import { NotFoundMiddleware } from "./middlewares/NotFoundMiddleware";
import { Logger } from "./common/Logger";
import { UserRoom } from "./game/rooms/UserRoom";
import { PartyRoom } from "./game/rooms/PartyRoom";

export class App {
  private app: express.Application;
  private port: number;
  private colyseus: Server;
  private routes: Routes[];

  constructor() {
    this.app = express();
    this.port = Number(process.env.PORT) || 3000;
    this.routes = [new UserRoute()];

    this.ConnectDatabase();
    this.UseMiddlewares();
    this.UseRoutes(this.routes);
    this.UseMiddlewareHandler();
    this.UseColyseus();
  }

  public Listen() {
    this.colyseus.listen(this.port);
    Logger.info(`=================================`);
    Logger.info(`🚀 COLYSEUS AND API listening on the port ${this.port}`);
    Logger.info(`=================================`);
  }

  private ConnectDatabase() {
    let url = "";

    if (DB_USER && DB_PASSWORD) {
      url = `mongodb://${DB_USER}:${encodeURIComponent(
        DB_PASSWORD
      )}@${DB_HOST}:${DB_PORT}/${DB_NAME}`;
    } else {
      url = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`;
    }

    mongoose
      .connect(url, {
        autoIndex: true,
        connectTimeoutMS: 10000,
        socketTimeoutMS: 45000,
      })
      .then(() => {
        Logger.info("Mongoose default connection open!");
      });
  }

  private UseColyseus() {
    this.colyseus = new Server({
      server: createServer(this.app),
      presence: new RedisPresence(),
    });

    this.colyseus.define("user", UserRoom);
    this.colyseus.define("party", PartyRoom);
  }

  private UseMiddlewares() {
    // Middleware library
    this.app.use(cors({ origin: "*", credentials: true }));
    this.app.use(hpp());
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
  }

  private UseMiddlewareHandler() {
    // Middleware custom
    this.app.use(ErrorMiddleware);
    this.app.use(NotFoundMiddleware);
  }

  private UseRoutes(routes: Routes[]) {
    routes.forEach((route) => {
      this.app.use(`/api/${route.path}`, route.router);
    });

    this.app.use("/monitor", monitor());
  }
}
