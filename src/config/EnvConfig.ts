import { config } from "dotenv";
config({ path: ".env" });

export const {
  PORT,
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_PASSWORD,
  DB_USER,
  PARTY_MAX_SIZE,
  JWT_SECRET_KEY,
} = process.env;
