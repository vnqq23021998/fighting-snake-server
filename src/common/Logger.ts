import * as winston from "winston";
import "winston-daily-rotate-file";

const InfoTransport = new winston.transports.DailyRotateFile({
  dirname: "logs",
  filename: "Information.%DATE%.log",
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  maxFiles: 3,
  level: "debug",
  json: false,
});

const ErrorTransport = new winston.transports.DailyRotateFile({
  dirname: "logs",
  filename: "Error.%DATE%.log",
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  maxFiles: 3,
  handleExceptions: true,
  json: false,
  level: "error",
});

const Logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
    winston.format.printf(
      ({ timestamp, level, message }) => `${timestamp} ${level}: ${message}`
    )
  ),
  transports: [InfoTransport, ErrorTransport],
});

Logger.add(
  new winston.transports.Console({
    format: winston.format.combine(
      winston.format.splat(),
      winston.format.colorize()
    ),
  })
);

export { Logger };
