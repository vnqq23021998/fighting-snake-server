import { HttpStatusCode } from "axios";
import { Response } from "express";

export class Json<T> {
  public Code?: HttpStatusCode;
  public Message?: string;
  public Data?: T;
}

export const Success = <T>(res: Response<Json<T>>, data?: T, msg?: string) => {
  return res.status(HttpStatusCode.Ok).json({
    Code: HttpStatusCode.Ok,
    Message: msg ?? "Thành công",
    Data: data,
  } as Json<T>);
};

export const BadRequest = <T>(res: Response<Json<T>>, msg: string) => {
  return res.status(HttpStatusCode.BadRequest).json({
    Code: HttpStatusCode.BadRequest,
    Message: msg,
    Data: undefined,
  } as Json<T>);
};

export const Create = <T>(
  res: Response<Json<T>>,
  code: HttpStatusCode,
  msg: string,
  data: T
) => {
  return res.status(code).json({
    Code: code,
    Message: msg,
    Data: data,
  } as Json<T>);
};

export const GameCreate = <T>(code: HttpStatusCode, msg: string, data: T) => {
  return JSON.stringify({
    Code: code,
    Message: msg,
    Data: data,
  } as Json<T>);
};

export const GameSuccess = <T>(data?: T, msg?: string) => {
  return JSON.stringify({
    Code: HttpStatusCode.Ok,
    Message: msg ?? "Thành công",
    Data: data,
  } as Json<T>);
};

export const GameBadRequest = <T>(msg: string) => {
  return JSON.stringify({
    Code: HttpStatusCode.BadRequest,
    Message: msg,
    Data: undefined,
  } as Json<T>);
};
