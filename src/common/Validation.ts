import { ClassConstructor, plainToClass } from "class-transformer";
import { validateSync } from "class-validator";

export const ConvertAndValidation = <T>(
  plainValue: string,
  classType: ClassConstructor<T>
): T => {
  const objectInParse = JSON.parse(plainValue);
  const convertedObject = plainToClass(classType, objectInParse);

  const reference = validateSync(convertedObject as Object, {
    skipMissingProperties: false,
    whitelist: true,
    forbidNonWhitelisted: false,
  });

  if (reference.length === 0) {
    return convertedObject;
  } else {
    const message = reference
      .map((error) => Object.values(error.constraints))
      .join(", ");

    throw new Error(message);
  }
};

export const ConvertToClass = <T>(
  plainValue: string,
  classType: ClassConstructor<T>
): T => {
  const objectInParse = JSON.parse(plainValue);
  const convertedObject = plainToClass(classType, objectInParse);
  return convertedObject;
};
