export enum EUserState {
  Idle,
  Move,
}

export enum EItemType {
  Diamond,
  Stealth,
  Acceleration,
}
