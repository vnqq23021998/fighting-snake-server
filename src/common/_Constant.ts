export class Constant {
  public static REDIS_ALL_USERS = "all_users";
  public static REDIS_ALL_PARTIES = "all_parties";
}

export const RandomNumber = (min: number, max: number): number => {
  return Math.random() * (max - min) + min;
};
